/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969;

/**
 *
 * @author 11616969
 */
import java.util.Scanner;
import static pkg11616969.CustomMessage.Message;
public class Main {
    
    /**
     * @param args the command line arguments
     */
    
    public static int Multiplier(int num){ // param is brought, when the method was invoked.      
        return num * num * num; // calculating cube.
    }
    public static boolean isInteger(String str) { //Validation str param to be integer.
    try {
        Integer.parseInt(str); // If no errors are to appear, then the method returns true.
        return true;
    } catch (NumberFormatException nfe) {
        return false; // On case of any errors caused on moment of parsing, the program falls into catch section and false boolean is returned
    }
}
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in); // new instance of Scanner object
        String current; //determining string variable
        
        Message("Please enter two positive, whole numbers, first smaller then the second one: "); //To reduce lines of codes method is used, which prints out in console text param.
        
        Message("Please enter the first number: ");
        do{ // This loop allows to implement one action not depending on any conditions
            current = scan.nextLine(); // Assigning input value to the current String variable
            if(!isInteger(current)) // To reduce code lines, isInteger() is used to check whether current variable has only integer value. Cases such as "3 3 3 3" and string inputs will cause try catch exception.
            {
                Message("Please enter whole number: ");
            }
        }
        while(!isInteger(current)); // Loop repeated over and over, while a user inputs valid integer value.
        
        int firstIntNumber = Integer.parseInt(current); // Saving first number input in a variable called firstIntNumber.
        
        
        Message("Please enter the second number: ");
        do{
            current = scan.nextLine();
            if(!isInteger(current))
            {
                Message("Please enter whole number: ");
            }
        }
        while(!isInteger(current));
        int secondIntNumber = Integer.parseInt(current);
        
        
        while(secondIntNumber < firstIntNumber){ // Checking whether the second number is higher than the first one. In case if num1 is higher it falls into the condition loop.
        Message("Second number must be higher then first number. Please re enter the last number: ");
        current = scan.next(); //Expecting value input after message
        while(!isInteger(current)) // Validating the input to be numerical string
        {
            Message("Please provide valid value: "); // On loop fall in, message asked
            current = scan.next(); // and input is being expected
        }
            secondIntNumber = Integer.parseInt(current); // Storing the new value entered after message warning alongwith casting it into integer value from string.
        }
        
        for(int i = firstIntNumber; i <= secondIntNumber; i++){ // loop works, till incremented by each cycle i value obeys conditions of the for loop.
            int cube = Multiplier(i); // param i wents into Multiplier method and returned as int. After it is stored in cube variable.
            Message("The cube of the first number " + i + " is - " + cube); // Displaying final result.
        }
    }
    
}
